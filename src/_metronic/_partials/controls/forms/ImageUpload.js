import React, { useState, useEffect} from "react";
import {config} from "../../../../config"
import SVG from "react-inlinesvg"
import {toAbsoluteUrl} from "../../../_helpers"
// import '../../../_assets/css/component.css'

const ImageUpload = props => {
    const API_URL = `${config.api_host}/uploads`;
    const FILE_FORMAT = ["image/svg", "image/jpg", "image/jpeg"];

    const [file, setFile] = useState();
    const [previewUrl, setPreviewUrl] = useState();
    const IMAGE_URL = props.imgUrl
    console.log("this is image url: " + props.imgUrl)
    useEffect(() => {
        if(IMAGE_URL !== false){
            setPreviewUrl(`${API_URL}/${IMAGE_URL}`)
        }

        if(!file){
            return
        }

        const fileReader = new FileReader();
        fileReader.onload = () => {
            setPreviewUrl(fileReader.result);
        };
        fileReader.readAsDataURL(file);
    }, [IMAGE_URL, file])


    const pickedHandler = (event) => {
        let pickedFile;
        if(event.target.files && event.target.files.length === 1){
            pickedFile = event.target.files[0]
            setFile(pickedFile)
        }
        props.onInput(pickedFile)
    };

    console.log("This is preview URL: " + previewUrl)
    return(
        <>
            <div className="text-center pt-3">
                <label>Upload {props.title}</label>
            </div>
            <div className="text-center center pt-5 row">
                { previewUrl && <div className="col-12"><img src={previewUrl} alt="Preview"/></div>  }
                { !previewUrl && <div className="col-12"><span className="svg-icon svg-icon-xl svg-icon-primary"> <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Image.svg")}/></span></div>  }
                { !previewUrl && <div className="col-12 pt-2"><label>Upload a file - PNG, JPG, SVG up to 2,5MB</label></div> }
               <div className="col-12 pt-5">
                   <label className="info" for="file-upload">
                       <span className="svg-icon svg-icon-lg svg-icon-info"><SVG src={toAbsoluteUrl("/media/svg/icons/Files/Upload.svg")}/> Choose File</span>
                       <input
                           id="file-upload"
                           className="sr-only"
                           type="file"
                           onChange={pickedHandler}
                       />
                   </label>

               </div>
            </div>


        {/*<div className="image-upload">*/}
        {/*    <div className="image-upload-preview align-center">*/}
        {/*        { previewUrl && <img src={previewUrl} alt="Preview"/> }*/}
        {/*        { !previewUrl && <h4>Please pick an image</h4>}*/}
        {/*        {}*/}

        {/*    </div>*/}
        {/*    <input*/}
        {/*        className="center form-control"*/}
        {/*        id={props.id}*/}
        {/*        type="file"*/}
        {/*        onChange={pickedHandler}*/}
        {/*    />*/}

        {/*</div>*/}
            </>
    )
}

export default ImageUpload