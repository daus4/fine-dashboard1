import * as React from 'react';
import { useHistory} from 'react-router-dom';
import {useDispatch} from 'react-redux';

import { userLogout } from '../_redux/authActions';
import { logout } from '../_redux/authCrud'

export default function Logout(){
  let history = useHistory();
  let dispatch = useDispatch();

  React.useEffect(() => {
    logout()
        .then(() =>  dispatch(userLogout()))
        .then(() => history.push('/auth/login'));
  }, [history, logout])

  return <div/>
}
