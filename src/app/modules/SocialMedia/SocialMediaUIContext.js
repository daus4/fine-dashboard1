import React, {createContext, useContext, useState} from "react";


const SocialMediaUIContext = createContext();

export function useSocialMediaUIContext(){
    return useContext(SocialMediaUIContext);
}

export const SocialMediaUIConsumer = SocialMediaUIContext.Consumer;

export function SocialMediaUIProvider({socialMediaUIEvents, children}){
    const [ids, setIds] = useState([]);

    const initSocialMedia = {
        platform: "",
        link: "",
        image: null
    }

    const value = {
        ids,
        setIds,
        initSocialMedia,
        newSocialMediaButtonClick: socialMediaUIEvents.newSocialMediaButtonClick,
        openEditSocialMediaDialog: socialMediaUIEvents.openEditSocialMediaDialog,
        openDeleteSocialMediaDialog: socialMediaUIEvents.openDeleteSocialMediaDialog,
    };

    return <SocialMediaUIContext.Provider value={value}>{children}</SocialMediaUIContext.Provider>
}