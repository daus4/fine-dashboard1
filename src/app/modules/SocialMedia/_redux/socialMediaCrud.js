import axios from "axios";
import {config} from "../../../../config";

const SOCIAL_MEDIA_URL = `${config.api_host}/api/social-media`

export function getSocialMedia(params){
    return axios.get(SOCIAL_MEDIA_URL, params);
}

export function getSocialMediaId(id){
    return axios.get(`${SOCIAL_MEDIA_URL}/${id}`);
}

export function createSocialMedia(socialMedia) {
    let data = new FormData();
    data.append("platform", socialMedia.platform);
    data.append("link", socialMedia.link);
    data.append("image", socialMedia.fileUploaded);
    // return console.log(data);
    return axios.post(SOCIAL_MEDIA_URL, data, {headers: {'Content-type': 'multipart/form-data'}});
}

export function updateSocialMedia(socialMedia){
    let data = new FormData()
    data.append("platform", socialMedia.platform);
    data.append("link", socialMedia.link);
    data.append("image", socialMedia.fileUploaded);
    return axios.put(`${SOCIAL_MEDIA_URL}/${socialMedia.social_media_id}`, data, {headers: {'Content-type': 'multipart/form-data'}} );
}

export function deleteSocialMedia(socialMediaId){
    return axios.delete(`${SOCIAL_MEDIA_URL}/${socialMediaId}`);
}