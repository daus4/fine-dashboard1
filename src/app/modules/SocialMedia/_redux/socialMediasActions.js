import * as requestFromServer from "./socialMediaCrud";
import {socialMediasSlice, callTypes} from './socialMediasSlice';

const {actions} = socialMediasSlice;

export const createSocialMedia = socialMediaForCreation => dispatch => {
    dispatch(actions.startCall({callType: callTypes.list}));
    return requestFromServer
        .createSocialMedia(socialMediaForCreation)
        .then(response => {
            const socialMedia = response.data;
            dispatch(actions.socialMediaCreated({ socialMedia }));
        })
        .catch(error => {
            error.clientMessage = "Can't create social media";
            dispatch(actions.catchError({error, callType: callTypes.action}))
        })
}

export const updateSocialMedia = socialMedia => dispatch => {
    console.log('====THIS IS REQUEST: =====');
    console.log(socialMedia);
    dispatch(actions.startCall({callType: callTypes.list}));
    return requestFromServer
        .updateSocialMedia(socialMedia)
        .then(response => {
            const socialMedia = response.data.socialMedia
            console.log('====THIS IS RESPONSE: =====')
            console.log(socialMedia)
            dispatch(actions.socialMediaUpdated({socialMedia}));
        })
        .catch(error => {
            error.clientMessage = "Can't update social media";
            dispatch(actions.catchError({error, callType: callTypes.action}));
        })
}

export const fetchSocialMedias = queryParams => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.list }));
    return requestFromServer
        .getSocialMedia(queryParams)
        .then(response => {
            const {count, data} = response.data;
            dispatch(actions.socialMediasFetched({count, data}))
        })
        .catch(error => {
            error.clientMessage = "Cant find social medias";
            dispatch(actions.catchError({error, callType: callTypes.list}))
        })
}

export const fetchSocialMedia = id => dispatch => {
    if(!id){
        return dispatch(actions.socialMediaFetched({socialMediaForEdit: undefined}));
    }

    dispatch(actions.startCall({callType: callTypes.action}))
    return requestFromServer
        .getSocialMediaId(id)
        .then(response => {
            const socialMedia = response.data;
            dispatch(actions.socialMediaFetched({socialMediaForEdit: socialMedia}));
        })
        .catch(error => {
            error.clientMessage = "Can't find social media with that id";
            dispatch(actions.catchError({error, callType: callTypes.list}));
        })
}

export const deleteSocialMedia = id => dispatch => {
    dispatch(actions.startCall({callType: callTypes.action}));
    return requestFromServer
        .deleteSocialMedia(id)
        .then( response => {
            dispatch(actions.socialMediaDeleted({ id }));
        })
        .catch(error => {
            error.clientMessage = "Can't find social media";
            dispatch(actions.catchError({error, callType: callTypes.list}));
        })
}