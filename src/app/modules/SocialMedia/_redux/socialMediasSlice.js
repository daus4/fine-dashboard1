import {createSlice} from "@reduxjs/toolkit";

const initialSocialMediasState = {
    listLoading: false,
    actionsLoading: false,
    count: 0,
    data: null,
    socialMediaForEdit: undefined,
    lastError: null
}

export const callTypes = {
    list: "list",
    action: "action"
};

export const socialMediasSlice = createSlice({
    name: "socialMedia",
    initialState: initialSocialMediasState,
    reducers: {
        catchError: (state, action) => {
            state.error = `${action.type}: ${action.payload.error}`;
            if (action.payload.callType === callTypes.list) {
                state.listLoading = false;
            } else {
                state.actionsLoading = false;
            }
        },
        startCall: (state, action) => {
            state.error = null;
            if (action.payload.callType === callTypes.list) {
                state.listLoading = true;
            } else {
                state.actionsLoading = true;
            }
        },
        // getCustomerById
        socialMediaFetched: (state, action) => {
            state.actionsLoading = false;
            state.socialMediaForEdit = action.payload.socialMediaForEdit;
            state.error = null;
        },

        socialMediasFetched: (state, action) => {
            const { count, data } = action.payload;
            state.listLoading = false;
            state.error = null;
            state.data = data;
            state.count = count;
        },
        // createSocialMedia
        socialMediaCreated: (state, action) => {
            console.log(action)
            state.actionsLoading = false;
            state.error = null;
            state.data.push(action.payload.socialMedia);
        },

        socialMediaUpdated: (state, action) => {
            state.actionsLoading = false;
            state.error = null;
            state.data = state.data.map(data => {
                if (data.social_media_id === action.payload.socialMedia.social_media_id) {
                    console.log("===THIS IS DATA===")
                    console.log(action.payload.socialMedia)
                    return action.payload.socialMedia;
                }
                return data;
            })
        },

        socialMediaDeleted: (state, action) => {
            state.actionsLoading = false;
            state.error = null;
            state.data = state.data.filter(el => el.id !== action.payload.social_media_id);
        }

    }
});