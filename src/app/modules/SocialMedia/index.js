import React from "react";
import {Route, useHistory } from "react-router-dom";
import {SocialMediaUIProvider} from "./SocialMediaUIContext";
import {SocialMediaEditDialog} from "./pages/social-media-edit-dialog/SocialMediaEditDialog";
import {SocialMediaCard} from "./pages/SocialMediaCard";
import {SocialMediaDeleteDialog} from "./pages/social-media-delete-dialog/SocialMediaDeleteDialog";

export function SocialMediaPage(){
    const history = useHistory();
    const socialMediaUIEvents = {
        newSocialMediaButtonClick: () => {
            history.push("social-media/new");
        },
        openEditSocialMediaDialog: (id) => {
            history.push(`social-media/edit/${id}`);
        },
        openDeleteSocialMediaDialog: (id) => {
            history.push(`social-media/delete/${id}`);
        }
    }
    return (
        <SocialMediaUIProvider socialMediaUIEvents={socialMediaUIEvents}>
            <Route path="/social-media/new">
                {({history, match}) => (
                    <SocialMediaEditDialog
                        show={match != null}
                        onHide={() => {
                        history.push("/social-media")
                        }}
                    />
                )}
            </Route>
            <Route path="/social-media/edit/:id">
                {({history, match}) => (
                    <SocialMediaEditDialog
                        show={match != null}
                        id={match && match.params.id}
                        onHide={()=>{
                            history.push("/social-media")
                        }}
                    />
                )}
            </Route>
            <Route path="/social-media/delete/:id">
                {({history, match}) => (
                    <SocialMediaDeleteDialog
                        show={match != null}
                        id={match && match.params.id}
                        onHide={() => {
                            history.push("/social-media")}
                        }
                    />
                )}
            </Route>
            <SocialMediaCard />
        </SocialMediaUIProvider>

        );
}