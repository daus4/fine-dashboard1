import React, {useMemo} from "react";
import { Card, CardBody, CardHeader, CardHeaderToolbar } from "../../../../_metronic/_partials/controls";
import SocialMediaTable from './social-media-table/SocialMediaTable'
import {useSocialMediaUIContext} from "../SocialMediaUIContext"

export function SocialMediaCard(){
    const socialMediaUIContext = useSocialMediaUIContext();
    const socialMediaUIProps = useMemo(() => {
        return {
            ids: socialMediaUIContext.ids,
            newSocialMediaButtonClick: socialMediaUIContext.newSocialMediaButtonClick,
        }
    }, [socialMediaUIContext])
    return (
        <Card>
            <CardHeader title="Social Media List">
                <CardHeaderToolbar>
                    <button
                        type="button"
                        className="btn btn-primary"
                        onClick={socialMediaUIProps.newSocialMediaButtonClick}
                    >
                        New Social Media
                    </button>
                </CardHeaderToolbar>
            </CardHeader>
            <CardBody>
                <SocialMediaTable/>
            </CardBody>
        </Card>
    )
}