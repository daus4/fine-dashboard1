import React, { useEffect } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector} from "react-redux";
import {ModalProgressBar} from "../../../../../_metronic/_partials/controls";
import * as actions from "../../_redux/socialMediasActions";
import { useSocialMediaUIContext } from "../../SocialMediaUIContext";

export function SocialMediaDeleteDialog({id, show, onHide}){
    const socialMediaUIContext = useSocialMediaUIContext();
    const dispatch = useDispatch();
    const { isLoading } = useSelector(
        (state) => ({ isLoading: state.socialMedias.actionsLoading}),
        shallowEqual
    )

    useEffect(() => {
        if (!id) {
            onHide();
        }
    }, [id]);

    useEffect(() => {}, [isLoading, dispatch]);

    const deleteSocialMedia = () => {
        dispatch(actions.deleteSocialMedia(id)).then(() => {
            dispatch(actions.fetchSocialMedias());
            onHide();
        })
    };

    return (
        <Modal
            show={show}
            onHide={onHide}
            aria-labelledby="example-modal-sizes-title-lg"
        >
            { isLoading && <ModalProgressBar />}
            <Modal.Header closeButton>
                <Modal.Title id="example-modal-sizes-title-lg">
                    Delete Social Media
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {!isLoading && (
                    <span>Are you sure want to delete this Social Media</span>
                )}
                {isLoading && (
                    <span>Deleting Social Media ...</span>
                )}
            </Modal.Body>
            <Modal.Footer>
                <div>
                    <button
                        type="button"
                        onClick={onHide}
                        className="btn btn-light btn-elevate"
                    >
                        Cancel
                    </button>
                    <> </>
                    <button
                        type="button"
                        onClick={deleteSocialMedia}
                        className="btn btn-primary btn-elevate"
                    >
                        Delete
                    </button>
                </div>
            </Modal.Footer>
        </Modal>
    )
}