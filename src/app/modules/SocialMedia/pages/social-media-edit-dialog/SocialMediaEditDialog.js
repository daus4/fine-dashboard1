import React, {useMemo, useState, useEffect} from "react";
import { Modal } from "react-bootstrap";
import {useHistory} from "react-router-dom";
import {useSocialMediaUIContext} from "../../SocialMediaUIContext"
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import * as actions from "../../_redux/socialMediasActions";
import {SocialMediaEditDialogHeader} from "./SocialMediaEditDialogHeader";
import {SocialMediaEditForm} from "./SocialMediaEditForm";


export function SocialMediaEditDialog({ id, show, onHide }){
    const history = useHistory();
    const socialMediaUIContext = useSocialMediaUIContext()
    const socialMediaUIProps = useMemo(() => {
        return {
            initSocialMedia: socialMediaUIContext.initSocialMedia,
        }
    }, [socialMediaUIContext]);


    const dispatch = useDispatch();

    const { actionsLoading, socialMediaForEdit } = useSelector(
        (state) => ({
            actionsLoading: state.socialMedias.actionsLoading,
            socialMediaForEdit: state.socialMedias.socialMediaForEdit
        }),
        shallowEqual
    );

    useEffect(() => {
        dispatch(actions.fetchSocialMedia(id));
    },[id, dispatch]);

    const saveSocialMedia = (socialMedia) => {
        if(!id) {
            dispatch(actions.createSocialMedia(socialMedia)).then(() =>onHide());
        } else {
            console.log('clicked in dialog')
            console.log(socialMedia)
            dispatch(actions.updateSocialMedia(socialMedia)).then(() => onHide());
        }
    }

    return (
        <Modal
            size="lg"
            show={show}
            onHide={onHide}
            aria-labelledby="modal-sizes-title-lg"
        >
        <SocialMediaEditDialogHeader id={id} />
        <SocialMediaEditForm
            id={id}
            saveSocialMedia={saveSocialMedia}
            actionsLoading={actionsLoading}
            socialMedia={socialMediaForEdit || socialMediaUIProps.initSocialMedia}
            onHide={onHide}
        />
        </Modal>

    )
}