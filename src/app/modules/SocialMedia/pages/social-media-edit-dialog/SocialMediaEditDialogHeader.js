import React, {useEffect, useState} from "react";
import {shallowEqual, useSelector} from "react-redux";
import {Modal} from "react-bootstrap";
import {ModalProgressBar} from "../../../../../_metronic/_partials/controls";

export function SocialMediaEditDialogHeader({id}){
    const { actionsLoading, socialMediaForEdit } = useSelector(
        (state) => ({
            actionsLoading: state.socialMedias.actionsLoading,
            socialMediaForEdit: state.socialMedias.socialMediaForEdit
        }),
        shallowEqual
    );

    const [title, setTitle] = useState("");
    useEffect(() => {
        let _title = id ? "" : "New Social Media";
        if(socialMediaForEdit && id){
            _title = `Edit Social Media '${socialMediaForEdit.platform}'`
        }
        setTitle(_title);
    }, [socialMediaForEdit, actionsLoading]);

    return (
        <>
            {actionsLoading && <ModalProgressBar/>}
            <Modal.Header closeButton>
                <Modal.Title id="example-modal-sizes-title-lg">{title}</Modal.Title>
            </Modal.Header>
        </>
    )
}