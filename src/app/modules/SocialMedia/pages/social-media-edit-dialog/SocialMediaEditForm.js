import React, { useState} from "react";
import {Modal} from "react-bootstrap";
import {Formik, Form, Field} from "formik";
import * as Yup from "yup";
import {Input} from "../../../../../_metronic/_partials/controls";
import ImageUpload from "../../../../../_metronic/_partials/controls/forms/ImageUpload"




const SocialMediaEditSchema = Yup.object().shape({
    platform: Yup.string()
        .min(5, "Minimum 3 character")
        .max(50, "Maximum 50 character")
        .required("Platform is required"),
    link: Yup.string()
        .min(10, "Minimum 3 character")
        .max(100, "Maximum 100 character")
        .required("Link is required"),

})


export function SocialMediaEditForm({saveSocialMedia, socialMedia, actionsLoading, onHide, id}){
    const [fileUploaded, setFileUploaded] = useState('');
    const handleFileUpload = (file) => {
        setFileUploaded(file, fileUploaded)
    }
    const imgURL = !id ? false : socialMedia.img_url;

    return (
       <>
           <Formik
                enableReinitialize={true}
                initialValues={socialMedia}
                validationSchema={SocialMediaEditSchema}
                onSubmit={(values) => {
                    const allValues = {...values, fileUploaded}
                    saveSocialMedia(allValues)


                }}
           >
               {({ handleSubmit }) => (
                   <>
                       <Modal.Body className="overlay overlay-block cursor-default">
                           {actionsLoading && (
                               <div className="overlay-layer bg-transparent">
                                   <div className="spinner spinner-lg spinner-success" />
                               </div>
                           )}
                           <Form className="form form-label-right">
                               <div className="form-group row">
                                   <div className="col-lg-6 border rounded">
                                       <ImageUpload title="Social Media" id="image" onInput={handleFileUpload} imgUrl={imgURL}/>
                                   </div>
                                   <div className="col-lg-6">
                                       <div className="col-lg-12">
                                           <Field
                                               name="platform"
                                               component={Input}
                                               placeholder="Social Media Platform"
                                               label="platform"
                                           />
                                       </div>
                                       <div className="col-lg-12">
                                           <Field
                                               name="link"
                                               component={Input}
                                               placeholder="Social Media Link"
                                               label="platform"
                                           />
                                       </div>
                                   </div>

                               </div>
                           </Form>
                       </Modal.Body>
                       <Modal.Footer>
                           <button
                               type="button"
                               onClick={onHide}
                               className="btn btn-light btn-elevate"
                               >
                               Cancel
                           </button>
                           <button
                               type="button"
                               onClick={() => handleSubmit()}
                               className="btn btn-primary btn-elevate"
                               >
                               Save
                           </button>
                       </Modal.Footer>
                   </>
               )}

           </Formik>
       </>
    )
}