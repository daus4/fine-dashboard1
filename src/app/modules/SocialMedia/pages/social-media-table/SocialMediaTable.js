import React, {useMemo, useEffect} from "react";
import BootstrapTable from "react-bootstrap-table-next";
import { shallowEqual,useDispatch, useSelector} from "react-redux";
import {config} from "../../../../../config";
import * as actions from "../../_redux/socialMediasActions"
import { sortCaret, headerSortingClasses } from "../../../../../_metronic/_helpers"
import { useSocialMediaUIContext } from "../../SocialMediaUIContext"
import * as columnFormatters from "./column-formatters"

export default function SocialMediaTable(){

    let socialMedias = useSelector((state) => (state.socialMedias), shallowEqual);

    const socialMediaUIContext = useSocialMediaUIContext();
    const socialMediaUIProps = useMemo(() => {
        return {
            openEditSocialMediaDialog: socialMediaUIContext.openEditSocialMediaDialog,
            openDeleteSocialMediaDialog: socialMediaUIContext.openDeleteSocialMediaDialog,

        }
    }, [socialMediaUIContext])

    function imageFormatter(cell, row, rowIndex){
        return(
            <img src={`${config.api_host}/uploads/${[cell]}`} alt={`social-media-${[rowIndex + 1]}`}/>
        )
    }
    const dispatch = useDispatch();
    useEffect(()=>{
        dispatch(actions.fetchSocialMedias())
    }, [dispatch]);

    const columns = [
        {
            dataField: "#",
            text: "ID",
            sort: true,
            sortCaret: sortCaret,
            headerS: sortCaret,
            headerSortingClasses,
            formatter: (cell, row, rowIndex) => {
                let rowNumber = rowIndex+1 ;
                return <span>{rowNumber}</span>

            }
        },
        {
            dataField: "platform",
            text: "PLATFORM",
            sort: true,
            headerS: sortCaret,
            headerSortingClasses,
        },
        {
            dataField: "link",
            text: "Social Media Link",
            sort: true,
            headerS: sortCaret,
            headerSortingClasses,
        },
        {
            dataField: "img_url",
            text: "Icon",
            formatter: imageFormatter
        },
        {
            dataField: "action",
            text: "Actions",
            formatter: columnFormatters.ActionsColumnFormatter,
            formatExtraData: {
                openEditSocialMediaDialog: socialMediaUIProps.openEditSocialMediaDialog,
                openDeleteSocialMediaDialog: socialMediaUIProps.openDeleteSocialMediaDialog
            }

        }


    ]

    const socialMedia = socialMedias.data

    return (
        <BootstrapTable
            wrapperClasses="table-responsive"
            bordered={false}
            classes="table table-head-custom table-vertical-center overflow-hidden"
            bootstrap4
            remote
            keyField="social_media_id"
            data={socialMedia === null ? [] : socialMedia}
            columns={columns}>

        </BootstrapTable>
        )
}