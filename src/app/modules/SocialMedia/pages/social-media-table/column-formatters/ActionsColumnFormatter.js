import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../../../_metronic/_helpers"

export function ActionsColumnFormatter(
    cellContent,
    row,
    rowIndex,
    { openEditSocialMediaDialog, openDeleteSocialMediaDialog }
){
    return (
        <>
            <a
                title="Edit Social Media"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => openEditSocialMediaDialog(row.social_media_id)}
            >
                <span className="svg-icon svg-icon-md svg-icon-primary">
                    <SVG src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}/>
                </span>
            </a>
            <a
                title="Delete Social Media"
                className="btn btn-icon btn-light btn-hover-danger btn-sm mx-3"
                onClick={() => openDeleteSocialMediaDialog(row.social_media_id)}
            >
                <span className="svg-icon svg-icon-md svg-icon-danger">
                    <SVG src={toAbsoluteUrl("media/svg/icons/General/Trash.svg")} />
                </span>
            </a>
        </>
    )
}