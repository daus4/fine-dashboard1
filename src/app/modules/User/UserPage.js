import React, {Suspense} from "react";
import { HashRouter as Route, Redirect, Switch} from "react-router-dom";
import {UserTablePage} from "./index";
import {UserEditPage} from "./pages/user-edit-page/UserEditPage";
import {LayoutSplashScreen, ContentRoute} from "../../../_metronic/layout";

export default function UserPage(){
    return (
        <Suspense fallback={<LayoutSplashScreen />}>
            <Switch>
                {
                    /* Redirect from root URL to /dashboard. */
                    <Redirect exact from="/" to="/user" />
                }

                <ContentRoute path="/user/new" component={UserEditPage} />
                <ContentRoute path="/user/edit/:id" component={UserEditPage} />
                <ContentRoute path="/user" component={UserTablePage} />
            </Switch>
        </Suspense>
    )
}