import React, { createContext, useContext, useState, useCallback } from "react";

const UsersUIContext = createContext();

export function useUsersUIContext(){
    return useContext(UsersUIContext);
}

export const UsersUIConsumer = UsersUIContext.Consumer;

export function UserUIProvider({usersUIEvents, children}){
    const [ids, setIds] = useState([]);

    // const initUser = {
    //     first_name: "",
    //     last_name: "",
    //     date_of_birth: "",
    //     phone_number: 1,
    //     email: "",
    //     role: "",
    // };

    const value = {
        ids,
        setIds,
        newUserButtonClick: usersUIEvents.newUserButtonClick,
        openEditUserPage: usersUIEvents.openEditUserPage,
        openDeleteUserDialog: usersUIEvents.openDeleteUserDialog
    };

    return (
        <UsersUIContext.Provider value={value}>{children}</UsersUIContext.Provider>
    )
}