export const UserStatusCssClasses = ["danger", "success"];
export const UserStatusTitles = ["Inactive", "Active"];
export const UserGenderCssClasses = ["primary", "info"];
export const UserGenderTitles = ["Male", "Female"];
export const UserRoles = ["root", "admin", "internship"];