import * as requestFromServer from './UsersCrud';
import {usersSlice, callTypes} from './UsersSlice';

const {actions} = usersSlice;

export const fetchUsers = queryParams => dispatch => {
    dispatch(actions.startCall({callTypes: callTypes.list}));
    return requestFromServer
        .getUsers(queryParams)
        .then(response => {
            const {count, data} = response.data;
            dispatch(actions.usersFetched({count, data}));
        })
        .catch(error => {
            error.clientMessage = "Can't find users";
            dispatch(actions.catchError({error, callType: callTypes.list}));
        })
}

export const fetchUser = id => dispatch => {
    if(!id){
        return dispatch(actions.userFetched({userForEdit: undefined}));
    }

    dispatch(actions.startCall({callTypes: callTypes.list}));
    return requestFromServer
        .getUser(id)
        .then(response => {
            const user = response.data.user;
            dispatch(actions.userFetched({userForEdit: user}));
        })
        .catch(error => {
            error.clientMessage = "Can't find social media with that id";
            dispatch(actions.catchError({error, callType: callTypes.list}));
        })
}

export const deleteUser = id => dispatch => {
    dispatch(actions.startCall({callType: callTypes.action}));
    return requestFromServer
        .deleteUser(id)
        .then( response => {
            dispatch(actions.userDeleted({id}));
        })
        .catch(error => {
            error.clientMessage = "Can't find user";
            dispatch(actions.catchError({error, callType: callTypes.list}));
        })
}

export const createUser = userForCreation => dispatch => {
    dispatch(actions.startCall({callType: callTypes.list}));
    console.log(userForCreation)
    return requestFromServer
        .createUser(userForCreation)
        .then(response => {
            const user = response.data;
            dispatch(actions.userCreated({user}));
        })
        .catch(error => {
            error.clientMessage = "Can't create user";
            dispatch(actions.catchError({error, callType: callTypes.action}));
        });
}

export const updateUser = user => dispatch => {
    dispatch(actions.startCall({callType: callTypes.list}));
    return requestFromServer
        .updateUser(user)
        .then(response => {
            const user = response.data.user;
            dispatch(actions.userUpdated({user}));
        })
        .catch(error => {
            error.clientMessage = "Can't update user";
            dispatch(actions.catchError({error, callType: callTypes.action}))
        })

}