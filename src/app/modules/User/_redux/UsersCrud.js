import axios from 'axios';
import {config} from "../../../../config";

const USERS_URL = `${config.api_host}/api/users`;
const AUTH_URL = `${config.api_host}/auth`
const USER_ROLES_URL = `${config.api_host}/api/role`;

export function getUsers(params){
    return axios.get(USERS_URL,params);
}

export function getUser(userId){
    return axios.get(`${USERS_URL}/${userId}`);
}

export function createUser(user){
    let data = new FormData();
    data.append("first_name", user.first_name);
    data.append("last_name", user.last_name);
    data.append("date_of_birth", user.date_of_birth);
    data.append("phone_number", user.phone_number);
    data.append("email", user.email);
    data.append("password", user.password);
    data.append("role", user.role);
    data.append("gender", user.gender);
    data.append("address", user.address);
    data.append("user_photo", user.fileUploaded)
    return axios.post(`${AUTH_URL}/register`, data, {headers: {'Content-type': 'multipart/form-data'}});
}

export function updateUser(user){
    let data = new FormData();
    data.append("first_name", user.first_name);
    data.append("last_name", user.last_name);
    data.append("date_of_birth", user.date_of_birth);
    data.append("phone_number", user.phone_number);
    data.append("email", user.email);
    data.append("password", user.password);
    data.append("role", user.role);
    data.append("gender", user.gender);
    data.append("address", user.address);
    data.append("user_photo", user.fileUploaded)
    return axios.put(`${AUTH_URL}/update/${user.user_id}`, data, {headers: {'Content-type':'multipart/form-data'}});
}

export function deleteUser(userId){
    return axios.delete(`${USERS_URL}/${userId}`);
}

export function getUserRoles(){
    return axios.get(USER_ROLES_URL)
}