import {createSlice} from "@reduxjs/toolkit";

const initialUsersState = {
    listLoading: false,
    actionsLoading: false,
    count: 0,
    data: null,
    userForEdit: undefined,
    lastError: null,
};

export const callTypes = {
    list: "list",
    action: "action",
};

export const usersSlice = createSlice({
    name: "user",
    initialState: initialUsersState,
    reducers: {
        catchError: (state, action) => {
            state.error = `${action.type}: ${action.payload.error}`;
            if(action.payload.callType === callTypes.list){
                state.listLoading = false;
            } else {
                state.actionsLoading = false;
            }
        },
        startCall: (state, action) => {
            state.error = null;
            if(action.payload.callType === callTypes.list){
                state.listLoading = true;
            } else {
                state.actionsLoading = true;
            }
        },
        usersFetched: (state, action) => {
            const { count, data } = action.payload;
            state.actionsLoading = false;
            state.listLoading = false;
            state.error = null;
            state.data = data;
            state.count = count;
        },
        userFetched: (state, action) => {
            state.actionsLoading = false;
            state.userForEdit = action.payload.userForEdit;
            state.error = null;
        },
        userDeleted: (state, action) => {
            state.actionsLoading = false;
            state.error = null;
            state.data = state.data.filter(el => el.id !== action.payload.user_id);
        },
        userCreated: (state, action) => {
            state.actionsLoading = false;
            state.error = null;
            state.data.push(action.payload.user);
        },
        userUpdated: (state, action) => {
            state.actionsLoading = false;
            state.error = null;
            state.data = state.data.map(data => {
                if (data.user_id === action.payload.user.user_id){
                    return action.payload.user
                }
                return data
            })
        }
    }
})