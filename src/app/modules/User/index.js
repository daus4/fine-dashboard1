import React from "react";
import {Route, useHistory} from "react-router-dom";
import {UserUIProvider} from "./UsersUIContext";
import {UsersCard} from "./pages/UsersCard";
import {UserDeleteDialog, UsersDeleteDialog} from "./pages/user-delete-dialog/UserDeleteDialog";


export function UserTablePage(){
    const history = useHistory();
    const usersUIEvents = {
        newUserButtonClick: () => {
            history.push("/user/new");
        },
        openEditUserPage: (id) => {
            history.push(`/user/edit/${id}`);
        },
        openDeleteUserDialog: (id) => {
            history.push(`/user/delete/${id}`);
        }
    }

    return (
        <UserUIProvider usersUIEvents={usersUIEvents}>
            <Route path="/user/delete/:id">
                {({history, match}) => (
                    <UserDeleteDialog 
                        show={match != null}
                        id={match && match.params.id}
                        onHide={() => {
                            history.push("/user")
                        }}
                    />
                )}
            </Route>
            <UsersCard />
        </UserUIProvider>
    )
}