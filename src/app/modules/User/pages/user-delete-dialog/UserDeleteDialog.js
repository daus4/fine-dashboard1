import React, {useEffect} from "react";
import {Modal} from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import {ModalProgressBar} from "../../../../../_metronic/_partials/controls";
import * as actions from "../../_redux/UsersActions";

export function UserDeleteDialog({id, show, onHide}){
    const dispatch = useDispatch();
    const { isLoading } = useSelector(
        (state) => ({ isLoading: state.users.actionsLoading}),
        shallowEqual
    )

    useEffect(() => {
        if(!id){
            onHide();
        }
    }, [id]);

    useEffect(()=> {}, [isLoading, dispatch]);

    const deleteUser = () => {
        dispatch(actions.deleteUser(id))
        .then(() => {
            dispatch(actions.fetchUsers());
            onHide();
        })
    }

    return (
        <Modal
            show={show}
            onHide={onHide}
            aria-labelledby="example-modal-sizes-title-lg"
        >
            { isLoading && <ModalProgressBar/>}
            <Modal.Header closeButton>
                <Modal.Title id="example-modal-sizes-title-lg">
                    Delete User
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {!isLoading && (
                    <span>Are you sure want to delete this user</span>
                )}
                {isLoading && (
                    <span>Deleting User ...</span>
                )}
            </Modal.Body>
            <Modal.Footer>
            <div>
                    <button
                        type="button"
                        onClick={onHide}
                        className="btn btn-light btn-elevate"
                    >
                        Cancel
                    </button>
                    <> </>
                    <button
                        type="button"
                        onClick={deleteUser}
                        className="btn btn-primary btn-elevate"
                    >
                        Delete
                    </button>
                </div>
            </Modal.Footer>
        </Modal>
    )
}