import React, {useState} from "react";
import {Formik, Form, Field} from "formik";
import * as Yup from "yup";
import {Input, Select, DatePickerField} from "../../../../../_metronic/_partials/controls";
import { UserRoles } from "../../UsersUIHelpers";
import ImageUpload from "../../../../../_metronic/_partials/controls/forms/ImageUpload"


const UserEditSchema = Yup.object().shape({
    first_name: Yup.string()
        .min(2, "Minimum 2 symbols")
        .max(50, "Maximum 50 symbols")
        .required("First Name is required"),
    last_name: Yup.string()
        .min(2, "Minimum 2 symbols")
        .max(50, "Maximum 50 symbols")
        .required("Last Name is required"),
    date_of_birth: Yup.mixed()
        .nullable(false)
        .required("Date of Birth is required"),
    phone_number: Yup.number()
        .min(6, "Minimum length of phone number is 6")
        .max(15, "Maximum length of phone number is 15")
        .required("Phone Number is required"),
    email: Yup.string().email().required("Email is required"),
    password: Yup.string()
        .min(6, "Minimum password is 6")
        .required("Password is required"),
    passwordConfirmation: Yup.string()
        .oneOf([Yup.ref('password'), null], 'Password did not match'),
    address: Yup.string().required("Address is required"),
})

export function UserEditForm({ id, user, btnRef, saveUser }){
    const [fileUploaded, setFileUploaded] = useState('');
    const handleFileUpload =  (file) => {
        setFileUploaded(file, fileUploaded);
    }

    const imgURL = !user ? false : user.user_photo_url;
    console.log("this is image url: " + user.user_photo_url)

    return (
        <>
            <Formik
                enableReinitialize={true}
                initialValues={user}
                validationSchema={UserEditSchema}
                onSubmit={(values) => {
                    const allValues = {...values, fileUploaded}
                    saveUser(allValues);
                }}
            >
                {({ handleSubmit }) => (
                    <>
                        <Form className="form form-label-right">
                            <div className="form-group row">
                                <div className="col-lg-4">
                                    <Field
                                        name="first_name"
                                        component={Input}
                                        placeholder="First Name"
                                        label="First Name"
                                    />
                                </div>
                                <div className="col-lg-4">
                                    <Field
                                        name="last_name"
                                        component={Input}
                                        placeholder="Last Name"
                                        label="Last Name"
                                    />
                                </div>
                                <div className="col-lg-4">
                                    <DatePickerField
                                        name="date_of_birth"
                                        label="Date of Birth"
                                    />
                                </div>
                            </div>
                            <div className="form-group row mb-3">
                                <div className="col-lg-4">
                                    <Field
                                        name="email"
                                        component={Input}
                                        placeholder="Email"
                                        label="Email"
                                    />
                                </div>
                                <div className="col-lg-4">
                                    <Field
                                        name="password"
                                        component={Input}
                                        placeholder="Password"
                                        label="Password"
                                        type="password"
                                    />
                                </div>
                                <div className="col-lg-4">
                                    <Field
                                        name="passwordConfirmation"
                                        component={Input}
                                        placeholder="Password Confirmation"
                                        label="Password Confirmation"
                                        type="password"
                                    />
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-lg-4">
                                   <label>Address</label> 
                                    <Field
                                        name="address"
                                        as="textarea"
                                        className='form-control'
                                    />
                                </div>
                                <div className="col-lg-4">
                                    <Field
                                        name="phone_number"
                                        component={Input}
                                        placeholder="Phone Number"
                                        label="Phone Number"
                                    />
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-lg-4">
                                    <Select name="role" label="Role">
                                        {UserRoles && UserRoles.map((role) => (
                                            <option key={role} value={role}>
                                                {role}
                                            </option>
                                        ))}
                                    </Select>
                                </div>
                                <div className="col-lg-6">
                                    <label>Gender</label>
                                    <div className="col">
                                        <div className="form-check-inline">
                                            <Field className="form-check-input" type="radio" name="gender" value="0" checked/>
                                            <label className="form-check-label" htmlFor="gender">Male</label>                
                                        </div>
                                        <div className="form-check-inline">
                                            <Field className="form-check-input" type="radio" name="gender" value="1"/>
                                            <label className="form-check-label" htmlFor="gender">Female</label>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-lg">
                                    <ImageUpload title="User" id="image" onInput={handleFileUpload} imgUrl={imgURL} />
                                </div>
                            </div>
                            <button
                                type="submit"
                                style={{display: "none"}}
                                ref={btnRef}
                                onSubmit={() => handleSubmit()}
                            ></button>
                        </Form>
                    </>
                )}

            </Formik>
        </>
    )
}