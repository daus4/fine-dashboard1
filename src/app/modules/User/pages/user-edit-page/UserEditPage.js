import React, {useEffect, useState, useRef} from 'react';
import {useDispatch, shallowEqual, useSelector} from "react-redux";
import * as actions from "../../_redux/UsersActions";
import { Card, CardBody, CardHeader, CardHeaderToolbar} from "../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls"
import { UserEditForm } from "./UserEditForm";

const initUser = {
    first_name: "",
    last_name: "",
    password: "",
    date_of_birth: "",
    phone_number: 123,
    email: "",
    address: "",
    gender: 0,
}


export function UserEditPage({history, match: {params: {id}}}){
    //Subheader
    const subheader = useSubheader();

    const [title, setTitle] = useState("");
    const dispatch = useDispatch();

    const { actionsLoading, userForEdit } = useSelector(
        (state) => ({
            actionsLoading: state.users.actionsLoading,
            userForEdit: state.users.userForEdit,
        }),
        shallowEqual()
    )

    useEffect(() => {
        dispatch(actions.fetchUser(id))
    }, [id, dispatch]);

    useEffect(() => {
        let _title = id ? "" : "New User";
        if (userForEdit && id) {
            _title = `Edit user - ${userForEdit.first_name} ${userForEdit.last_name}`;
        }

        setTitle(_title);
        subheader.setTitle(_title)

    }, [userForEdit, id]);

    const saveUser = (values) => {
        if(!id){
            dispatch(actions.createUser(values)).then(() => backToUsersList());
        } else {
            dispatch(actions.updateUser(values)).then(() => backToUsersList());
        }
    }

    const btnRef = useRef();
    const saveUserClick = () => {
        if(btnRef && btnRef.current){
            btnRef.current.click();
        }
    }

    const backToUsersList = () => {
        history.push(`/user`)
    }

    return(
        <Card>
            {actionsLoading && <ModalProgressBar/> }
            <CardHeader title={title}>
                <CardHeaderToolbar>
                    <button
                        type="button"
                        onClick={backToUsersList}
                        className="btn btn-light"
                    >
                        <i className="fa fa-arrow-left"></i>
                        Back
                    </button>
                    {` `}
                    <button
                        type="submit"
                        className="btn btn-primary ml-2"
                        onClick={saveUserClick}
                    >
                        Save
                    </button>
                </CardHeaderToolbar>
            </CardHeader>
            <CardBody>
                <UserEditForm
                    actionsLoading={actionsLoading}
                    user={userForEdit || initUser}
                    btnRef={btnRef}
                    saveUser={saveUser}
                />
            </CardBody>
        </Card>
    )
}