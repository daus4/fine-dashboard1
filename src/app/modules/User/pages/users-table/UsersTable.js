import React, {useMemo, useEffect} from "react";
import BootstrapTable from "react-bootstrap-table-next";
import { shallowEqual,useDispatch, useSelector} from "react-redux";
import {config} from "../../../../../config";
import * as actions from "../../_redux/UsersActions";
import { useUsersUIContext } from "../../UsersUIContext";
import * as columnFormatters from "./column-formatters";

export default function UsersTable(){
    let users = useSelector((state) => (state.users), shallowEqual);
    console.log(users)
    const userUIContext = useUsersUIContext();
    const userUIProps = useMemo(() => {
        return {
            openEditUserPage: userUIContext.openEditUserPage,
            openDeleteUserDialog: userUIContext.openDeleteUserDialog,
        }
    }, [userUIContext]);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(actions.fetchUsers());
    }, [dispatch]);

    const columns = [
        {
            dataField: "#",
            text: "ID",
            formatter: (cell, row, rowIndex) => {
                let rowNumber = rowIndex+1 ;
                return <span>{rowNumber}</span>
            }
        },
        {
            dataField: "first_name",
            text: "FIRST NAME",
        },
        {
            dataField: "last_name",
            text: "LAST NAME",
        },
        {
            dataField: "email",
            text: "EMAIL",
        },
        {
            dataField: "gender",
            text: "GENDER",
            formatter: columnFormatters.GenderColumnFormatter,
        },
        {
            dataField: "status",
            text: "STATUS",
            formatter: columnFormatters.StatusColumnFormatter,
        },
        {
            dataField: "action",
            text: "Actions",
            formatter: columnFormatters.ActionsColumnFormatter,
            formatExtraData: {
                openEditUserPage: userUIProps.openEditUserPage,
                openDeleteUserDialog: userUIProps.openDeleteUserDialog,
            }
        }
    ]

    const user = users.data

    return (
        <BootstrapTable
            wrapperClasses="table-responsive"
            bordered={false}
            classes="table table-head-custom table-vertical-center overflow-hidden"
            bootstrap4
            remote
            keyField="social_media_id"
            data={user === null ? [] : user}
            columns={columns}>
        </BootstrapTable>
    )
}