/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import {OverlayTrigger, Tooltip} from "react-bootstrap"
import SVG from 'react-inlinesvg';
import { toAbsoluteUrl } from "../../../../../../_metronic/_helpers";

export function ActionsColumnFormatter(
    cellContent,
    row,
    rowIndex,
    {openEditUserPage, openDeleteUserDialog}
){
    return (
        <>
        <OverlayTrigger overlay={<Tooltip id="user-edit-tooltip">Edit User</Tooltip>}>
            <a
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={()=> openEditUserPage(row.user_id)}
            >
                <span className="svg-icon svg-icon-md svg-icon-primary">
                    <SVG src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")} />
                </span>
            </a>
        </OverlayTrigger>
        <OverlayTrigger overlay={<Tooltip id="user-delete-tooltip">Delete User</Tooltip>}>
            <a
        title="Delete User"
        className="btn btn-icon btn-light btn-hover-danger btn-sm mx-3"
        onClick={()=> openDeleteUserDialog(row.user_id)}
        >
                <span className="svg-icon svg-icon-md svg-icon-danger">
                    <SVG src={toAbsoluteUrl("media/svg/icons/General/Trash.svg")} />
                </span>
            </a>
        </OverlayTrigger>
        </>
    )
}