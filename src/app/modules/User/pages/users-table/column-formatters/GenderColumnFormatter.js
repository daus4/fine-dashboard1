import React from "react";
import {UserGenderCssClasses, UserGenderTitles} from "../../../UsersUIHelpers";

export const GenderColumnFormatter = (cellContent, row) => {
    return (
        <span
        className={`label label-lg label-light-${UserGenderCssClasses[row.gender]} label-inline`}
        >
            {UserGenderTitles[row.gender]}
        </span>   
    )
}