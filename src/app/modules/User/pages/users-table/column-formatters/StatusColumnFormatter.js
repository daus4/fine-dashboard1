import React from "react";
import {UserStatusCssClasses, UserStatusTitles} from "../../../UsersUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => {
    const userStatus = row.status === false ? 0 : 1;
    return (
        <span
        className={`label label-lg label-light-${UserStatusCssClasses[userStatus]} label-inline`}
        >
            {UserStatusTitles[userStatus]}
        </span>
    )
}
